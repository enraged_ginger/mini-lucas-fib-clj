(defproject mini-lucas-fib-clj "0.1.0-SNAPSHOT"
  :description "Does the lucas-fib sequence in the fewest bytes possible."
  :url "http://www.bitbucket.org/enraged_ginger/mini-lucas-fib-clj"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :main ^:skip-aot user
  :resource-paths ["lib/clojure-1.9.0-master.jar"]
  :target-path "target/%s"
  :aliases {"do-it" ["run" "-m" "user/g"]})
