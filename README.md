# mini-lucas-fib-clj

Runs the Lucas Fibonacci sequence using the fewest bytes possible.

## Installation

You can run the program by doing this (assuming you have Leiningen and the aforementioned Clojure JAR installed):
```
lein do-it 5
```

## License

Copyright © 2016 Stephen M. Hopper

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.